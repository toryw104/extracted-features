#!/usr/bin/env python3
#
# extract <meta-data> elements from AndroidManifest.xml to look for
# API Key idenfiers and other interesting tidbits.
# https://github.com/Exodus-Privacy/etip/issues/62

import bs4
import collections
import json
import os
import re
import sys
from androguard.core.bytecodes.apk import get_apkid
from bs4 import BeautifulSoup


class Encoder(json.JSONEncoder):
    dict_keys = type({}.keys())
    def default(self, obj):
        if isinstance(obj, set) or isinstance(obj, self.dict_keys):
            return sorted(obj)
        return super().default(obj)


APK_ROOT_DIR = '/data/malware-apks'

output = dict()
output['reserved-words'] = ('attributes', 'errors', 'meta-data', 'receivers')
output['errors'] = dict()
d = dict()
output['meta-data'] = d
attributes = set()
output['attributes'] = attributes
receivers = {
    'attributes': set(),
    'names': set(),
    'intent-filter-action-names': set(),
}
output['receivers'] = receivers
basedir = os.path.dirname(__file__)
datadir = os.path.join(basedir, 'apkparser-axml2xml')
outputdir = os.path.join(basedir, os.path.basename(__file__).replace('-run', ''))
os.makedirs(outputdir, exist_ok=True)
attributes_file = os.path.join(outputdir, 'attributes.json')
names_file = os.path.join(outputdir, 'names.json')
names_with_apk_paths_file = os.path.join(outputdir, 'names-with-apk-paths.json')
output_file = os.path.join(outputdir, 'meta-data.json')
receivers_file = os.path.join(outputdir, 'receivers.json')

print('Scanning', datadir)
i = 0
for root, dirs, files in os.walk(datadir):
    print(root)
    for f in files:
        if not f.endswith('.AndroidManifest.xml'):
            print('SKIPPING', os.path.join(root, f))
            continue
        path = os.path.join(root, f)
        apkpath = path[len(basedir) + 20:-20]
        statinfo = os.stat(path)
        axml_meta_data_output = {
            'AndroidManifest.xml_ExtractionTimes': {
                'ctime': statinfo.st_ctime,
                'mtime': statinfo.st_mtime,
            },
            'meta-data': [],
            'receiver': [],
        }
        with open(path) as fp:
            bs = BeautifulSoup(fp.read(), 'lxml')
            for meta_data in bs.find_all('meta-data'):
                attributes.update(meta_data.attrs)
                line = ''
                if 'android:name' not in meta_data.attrs.keys():
                    if f not in output['errors']:
                        output['errors'][apkpath] = []
                    output['errors'][apkpath].append(str(meta_data))
                    print('ERROR:', f, '<meta-data/> has no android:name:\n', meta_data)
                    continue
                name = meta_data.attrs['android:name']
                meta_data_output_dict = {'name': name}
                axml_meta_data_output['meta-data'].append(meta_data_output_dict)
                if name not in d:
                    d[name] = dict()
                if 'apks' not in d[name]:
                    d[name]['apks'] = set()
                d[name]['apks'].add(apkpath)  # relative path and rstrip .AndroidManifest.xml
                if 'android:resource' in meta_data.attrs:
                    if 'resources' not in d[name]:
                        d[name]['resources'] = set()
                    resource = meta_data.attrs['android:resource']
                    meta_data_output_dict['resource'] = resource
                    d[name]['resources'].add(resource)
                if 'android:value' in meta_data.attrs:
                    if 'values' not in d[name]:
                        d[name]['values'] = set()
                    value = meta_data.attrs['android:value']
                    meta_data_output_dict['value'] = value
                    d[name]['values'].add(value)

            for receiver in bs.find_all('receiver'):
                receivers['attributes'].update(receiver.attrs)
                if 'android:name' in receiver.attrs:
                    receivers['names'].add(receiver.attrs['android:name'])
                for intent_filter in receiver.find_all('intent-filter'):
                    for action in intent_filter.find_all('action'):
                        if 'android:name' in action.attrs:
                            receivers['intent-filter-action-names'].add(action.attrs['android:name'])
                            axml_meta_data_output['receiver'].append({
                                'name': receiver.attrs['android:name'],
                                'intent-filter-action-name': action.attrs['android:name'],
                            })

        axml_meta_data_output_file = os.path.join(outputdir, apkpath + '.axml-meta-data.json')
        os.makedirs(os.path.dirname(axml_meta_data_output_file), exist_ok=True)
        with open(axml_meta_data_output_file, 'w') as fp:
            json.dump(axml_meta_data_output, fp, indent=2, sort_keys=True)

    i += 1
    if i > 1000:
        i = 0
        with open(output_file, 'w') as fp:
            json.dump(output, fp, indent=2, sort_keys=True, cls=Encoder)
with open(output_file, 'w') as fp:
    json.dump(output, fp, indent=2, sort_keys=True, cls=Encoder)
with open(attributes_file, 'w') as fp:
    json.dump(sorted(attributes), fp, indent=2)
with open(receivers_file, 'w') as fp:
    json.dump(receivers, fp, indent=2, sort_keys=True, cls=Encoder)
with open(names_file, 'w') as fp:
    json.dump(sorted(d.keys()), fp, indent=2)
names_with_apk_paths = dict()
for k, v in d.items():
    names_with_apk_paths[k] = sorted(v['apks'])
with open(names_with_apk_paths_file, 'w') as fp:
    json.dump(names_with_apk_paths_file, fp, indent=2, sort_keys=True)
